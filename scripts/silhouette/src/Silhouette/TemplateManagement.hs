{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Werror=missing-fields #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.TemplateManagement where

import Control.Monad.Except
import Control.Monad.Identity
import Data.Aeson
import Data.Aeson.TH (deriveToJSON, defaultOptions)
import Data.Either.Combinators
import Data.Text (Text)
import GHC.Natural
import Paths_silhouette
import Text.Ginger

import qualified Data.HashMap.Lazy as HM
import qualified Data.Text as T
import qualified Silhouette.Readers.ApplicationSpec as AS

-- Ancillary types
data EnvVar =
    NormalEnvVar { name :: Text, value :: Text }
  | SecretEnvVar { name :: Text, secretName :: Text, secretKey :: Text }
    deriving (Show, Eq)

$(deriveToJSON defaultOptions ''EnvVar)

data SecretAtom = SecretAtom
  { name :: Text
  , value :: Text -- base64-encoded string
  } deriving (Show, Eq)

$(deriveToJSON defaultOptions ''SecretAtom)

data VolumeMount = VolumeMount
  { name :: Text
  , mountPath :: FilePath
  , subPath :: Maybe FilePath
  } deriving (Show, Eq)

data PersistentVolumeClaimSource = PVC
  { claimName :: Text
  } deriving (Show, Eq)

data KeyToPath = KeyToPath
  { key :: FilePath
  , path :: FilePath
  } deriving (Show, Eq)

data SecretVolumeSource = SVS
  { secretName :: Text
  , defaultMode :: Natural -- permission bits in decimal
  , items :: [KeyToPath]
  } deriving (Show, Eq)

data Volume = PvcVolume
  { name :: Text
  , pvc :: PersistentVolumeClaimSource
  } | SecretsVolume
  { name :: Text
  , secret :: SecretVolumeSource
  } deriving (Show, Eq)

data TlsSource =
    LeTlsResolver
  | LeHttpResolver
  | TlsSecret { name :: Text }
  deriving (Show, Eq)

$(deriveToJSON defaultOptions { omitNothingFields = True } ''VolumeMount)
$(deriveToJSON defaultOptions ''PersistentVolumeClaimSource)
$(deriveToJSON defaultOptions ''KeyToPath)
$(deriveToJSON defaultOptions ''SecretVolumeSource)

instance ToJSON Volume where
  toJSON PvcVolume{..} = object ["name" .= name , "persistentVolumeClaim" .= pvc]
  toJSON SecretsVolume{..} = object ["name" .= name, "secret" .= secret]

instance ToJSON TlsSource where
  toJSON LeTlsResolver = object ["certResolver" .= ("le-tls" :: Text)]
  toJSON LeHttpResolver = object ["certResolver" .= ("le-http" :: Text)]
  toJSON TlsSecret{name} = object ["secretName" .= name ]

data PersistentVolumeClaim = PersistentVolumeClaim
  { appName :: Text
  , appOwner :: Text
  , name :: Text
  , volume :: AS.Volume
  , environment :: Text
  } deriving (Show, Eq)

data PersistentVolume = PersistentVolume
  { appName :: Text
  , appOwner :: Text
  , name :: Text
  , volume :: AS.Volume
  , environment :: Text
  } deriving (Show, Eq)

data Deployment = Deployment
  { appName :: Text
  , appOwner :: Text
  , imagePullSecrets :: [Text]
  , imageName :: Text
  , initCommand :: Text
  , initImageName :: Text
  , port :: Natural
  , envvars :: [EnvVar]
  , mainDomain :: Text
  , volumeMounts :: [VolumeMount]
  , volumes :: [Volume]
  , probePath :: Text
  } deriving (Show, Eq)

data Secret = Secret
  { secretName :: Text
  , secrets :: [SecretAtom]
  } deriving (Show, Eq)

data Service = Service
  { appName :: Text
  , appOwner :: Text
  , port :: Natural
  } deriving (Show, Eq)

data IngressRoute = IngressRoute
  { ingressRouteName :: Text
  , appName :: Text
  , appOwner :: Text
  , routes :: [Text]
  , tlsSource :: TlsSource
  } deriving (Show, Eq)

$(deriveToJSON defaultOptions ''PersistentVolumeClaim)
$(deriveToJSON defaultOptions ''PersistentVolume)
$(deriveToJSON defaultOptions ''Deployment)
$(deriveToJSON defaultOptions ''Secret)
$(deriveToJSON defaultOptions ''Service)
$(deriveToJSON defaultOptions ''IngressRoute)

-- Wrap the type used by Ginger
type ParsedTemplate = Template SourcePos

-- AvalableTemplates
-- XXX: Use Kind to Value promotion?
data TemplateLib = TemplateLib
  { tDeployment   :: ParsedTemplate
  , tService      :: ParsedTemplate
  , tIngressRoute :: ParsedTemplate
  , tPVolume      :: ParsedTemplate
  , tPVolumeClaim :: ParsedTemplate
  , tSecret       :: ParsedTemplate
  } deriving Show

parseTemplate :: FilePath -> ExceptT String IO ParsedTemplate
parseTemplate filename = ExceptT $ parse <$> readFile filename
  where parse :: String -> Either String ParsedTemplate
        parse = mapLeft renderError . runIdentity . parseGinger' opts
        renderError = formatParserError (Just filename)
        noIncludes = const $ return Nothing -- Disable includes in templates
        opts = (mkParserOptions noIncludes) { poSourceName = Just filename
                                            , poKeepTrailingNewline = True
                                            }

mkTemplateLib :: ExceptT String IO TemplateLib
mkTemplateLib = do
  let loadTemplate resource = lift (getDataFileName resource) >>= parseTemplate
  tDeployment   <- loadTemplate "templates/Deployment.yml"
  tService      <- loadTemplate "templates/Service.yml"
  tIngressRoute <- loadTemplate "templates/IngressRoute.yml"
  tPVolume      <- loadTemplate "templates/PersistentVolume.yml"
  tPVolumeClaim <- loadTemplate "templates/PersistentVolumeClaim.yml"
  tSecret       <- loadTemplate "templates/Secret.yml"
  pure TemplateLib{..}

resolveKeys :: ToJSON templateCtx => templateCtx -> Text -> Maybe Value
resolveKeys templateCtx key = followPath (T.splitOn "." key) (toJSON templateCtx)
  where followPath :: [Text] -> Value -> Maybe Value
        followPath []     obj        = Just obj
        followPath (a:as) (Object o) = HM.lookup a o >>= followPath as
        followPath path obj = error ("Could not resolve property path " <> show path <> ":" <> show obj <>
                                     ". Full path: " <> show key)

renderTemplate :: ToJSON tv => ParsedTemplate -> tv -> Text
renderTemplate template value = runGinger context template
  where context = makeContextText (toGVal . resolveKeys value)
