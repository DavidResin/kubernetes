{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Werror=missing-fields #-}
{-# OPTIONS_GHC -fno-cse #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This program transpiles simple application specifications into applicable
-- Kubernetes deployments.

module Main where

import Control.Monad (forM_, when, zipWithM_)
import Control.Monad.Except (ExceptT, runExceptT)
import Data.Either.Combinators (maybeToRight)
import Data.Text (Text)
import Data.Text.IO (writeFile)
import Data.Yaml
import Prelude hiding (writeFile)
import Silhouette.Readers.ApplicationSpec (Application(..))
import Silhouette.Renderers.K8sDistribution (renderDistribution)
import Silhouette.Renderers.K8sSecrets (renderSecrets)
import Silhouette.Renderers.K8sRequirements (computeReqs, renderReqs)
import Silhouette.Processing
import Silhouette.Types (AppSecrets)
import System.Console.CmdArgs
import System.Directory
import System.Exit hiding (die)
import System.FilePath.Posix
import System.IO (hPutStrLn, stderr)

import qualified Data.Text as T
import qualified Silhouette.TemplateManagement as TM
import qualified Silhouette.Readers.SecretSpec as SS

data CmdModes =
    CmdMkApps
  { envName :: Text
  , specPath :: FilePath
  , destDir :: FilePath
  , mountDomain :: Text
  , useTld :: Bool
  , reqsFile :: Maybe FilePath
  , lockImages :: Maybe Text
  , tagImages :: Maybe Text
  }
  | CmdMkImageList
  { specPath :: FilePath
  , outputFile :: FilePath
  , lockImages :: Maybe Text
  , tagImages :: Maybe Text
  }
  | CmdMkSecrets
  { specPath :: FilePath
  , outputFile :: FilePath
  , useExamples :: Bool
  , secretsFile :: Maybe FilePath
  } deriving (Show, Data, Typeable)

mkAppsArgs = CmdMkApps
  { envName = "" &= argPos 0 &= typ "ENV_NAME"
  , specPath = def &= argPos 1 &= typFile &= typ "/path/to/spec.yml"
  , destDir = "./generated-configurations/" &= typDir
                &= help "Directory where to write the generated files."
  , mountDomain = "ageptest.ch" &= explicit &= name "domain" 
                &= help "Domain where to mount the applications. \
                \By default, the environment name will be prepended."
  , useTld = False &= explicit &=name "use-tld"
          &= help "If specified, the environment name will not be prepended to the domain."
  , reqsFile = Nothing &= opt ("requirements.txt" :: String) &= explicit &= name "reqs" &= typ "requirements.txt"
           &= help "Generate a list of requirements along with the configuration"
  -- By using impure annotations we cannot factor common options
  , lockImages = Nothing &= explicit &= groupname "Image locking" &= name "lock-images" &= typ "<registry_url>"
              &= help "Application images will be rewritten to <registry_url>/<appName>. If the \
                      \registry_url is empty, the local registry will be used."
  -- By using impure annotations we cannot factor common options
  , tagImages = Nothing &= explicit &= groupname "Image locking" &= name "tag-images" &= typ "<image_tag>"
             &= help "Force application images tag. Existing tags will be overwritten."
  } &= name "config" &= explicit
    &= help "Generate a Kubernetes configuration from an applications specification.\
            \The necessary resources will be allocated under the specified environment. \
            \This is just a name so that applications can find resources such as \
            \volume data across multiple generations of deployments. \
            \In the Gitlab-CI this is usually the branch name."

mkImageListArgs = CmdMkImageList
  { specPath = def &= argPos 1 &= typFile &= typ "/path/to/spec.yml"
  , outputFile = "./image-list.txt" &= help "Path where to write the secrets."
  -- By using impure annotations we cannot factor common options
  , lockImages = Nothing &= explicit &= groupname "Image locking" &= name "lock-images" &= typ "<registry_url>"
              &= help "Application images will be rewritten to <registry_url>/<appName>. If the \
                      \registry_url is empty, the local registry will be used."
  -- By using impure annotations we cannot factor common options
  , tagImages = Nothing &= explicit &= groupname "Image locking" &= name "tag-images" &= typ "<image_tag>"
             &= help "Force application images tag. Existing tags will be overwritten."
  } &= name "image-list" &= explicit
    &= help "Generate a CSV containing the application name, it's docker image and the image \
            \expected by the deployment after honoring locking options. See Image locking."

mkSecretsArgs = CmdMkSecrets
  { specPath = def &= argPos 1 &= typFile &= typ "/path/to/spec.yml"
  , outputFile = "./generated-configurations/secrets.yml" &= help "Path where to write the secrets."
  , useExamples = False &= groupname "Secret sources" &= explicit &= name "from-examples"
                    &= help "Construct secrets from the examples provided in the specification."
  , secretsFile = def &= groupname "Secret sources" &= explicit &= name "secrets-file"
                    &= help "Construct secrets from the specified file."
  } &= name "secrets" &= explicit
    &= help "Generate Kubernetes secrets for the applications in the supplied specification.\
            \Secrets can be generated from the examples specified in the configuration, or \
            \from a separate file defining every secret for each application. Note that \
            \a secret source MUST be specified."

die message exitCode reason =
  hPutStrLn stderr ("Error: " ++ message ++ ". " ++ reason)
  >> exitWith (ExitFailure exitCode)

loadTemplates :: IO TM.TemplateLib
loadTemplates = either onTemplateLoadError pure =<< runExceptT TM.mkTemplateLib
  where onTemplateLoadError = die "Could not load template" 131 . show

loadAppSpec :: FilePath -> IO [Application]
loadAppSpec specPath = either onAppSpecLoadError pure =<< decodeFileEither specPath
  where onAppSpecLoadError = die "Could not load Application specification" 11 . prettyPrintParseException

loadSecretsFile :: FilePath -> IO SS.SecretsFile
loadSecretsFile path = either onDetachedSecretSpecLoadError pure =<< decodeFileEither path
  where onDetachedSecretSpecLoadError = die "Could not load detached secret specification" 11 . prettyPrintParseException

loadSecretsSpec :: Maybe FilePath -> [Application] -> IO [AppSecrets]
loadSecretsSpec Nothing _ = die "Please specify a secrets source" 11 ""
loadSecretsSpec (Just filepath) apps = either onLoadError pure =<< mkAppSecrets =<< loadSecretsFile filepath
  where onLoadError = die "Could not load Secret specification" 11 . show
        mkAppSecrets sf = runExceptT $ mapM (secretsFromFile (takeDirectory filepath) sf) apps

handleCmd :: CmdModes -> IO ()
handleCmd CmdMkApps{..} = do
  let mkAppPath appName = joinPath [destDir, appName ++ ".yml"]
      processorOptions = ProcessorOptions {mountDomain, environment = envName, useTld, lockImages, tagImages}

  templates <- loadTemplates
  applications <- loadAppSpec specPath

  when (null applications) $ die "The specified description did not provide any applications!" 11 ""

  appSecrets <- mapM (secretsFromExamples (takeDirectory specPath)) applications

  let appCfgs = fmap (mkAppConfiguration processorOptions) applications
      appNames = fmap appName applications
      writeApplication appName appCfg = do
        let path = mkAppPath . T.unpack $ appName
        writeFile path $ renderDistribution templates appCfg
      writeReqsFile filename = do
        let path = joinPath [destDir, filename]
        writeFile path $ renderReqs $ mconcat $ zipWith computeReqs appCfgs appSecrets

  createDirectoryIfMissing True destDir
  zipWithM_ writeApplication appNames appCfgs

  forM_ reqsFile writeReqsFile

  putStrLn $ "Created " ++ show (length applications) ++ " application configurations."

handleCmd CmdMkSecrets{..} = do
  let writeSecrets :: TM.TemplateLib -> [AppSecrets] -> IO ()
      writeSecrets templateLib secretConfigs = writeFile outputFile text
        where text = renderSecrets templateLib secretConfigs

  templates <- loadTemplates
  applications <- loadAppSpec specPath

  when (null applications) $ die "The specified description did not provide any applications!" 11 ""

  secretsConfig <- if not useExamples then loadSecretsSpec secretsFile applications
                   else mapM (secretsFromExamples (takeDirectory specPath)) applications

  createDirectoryIfMissing True $ takeDirectory outputFile
  writeSecrets templates secretsConfig

  putStrLn $ "Processed secrets for " ++ show (length applications) ++ " applications."

handleCmd CmdMkImageList{..} = do
  let appImage Application{..} = lockImageName appName lockImages tagImages imageName
      appLine app@Application{..} = appName <> "," <> imageName <> "," <> appImage app
      writeImageList apps = writeFile outputFile text
        where text = T.unlines $ map appLine apps
  templates <- loadTemplates
  applications <- loadAppSpec specPath

  when (null applications) $ die "The specified description did not provide any applications!" 11 ""

  writeImageList applications

  putStrLn $ "Wrote image list for " ++ show (length applications) ++ " applications."


silhouetteModes = modes [mkAppsArgs, mkImageListArgs, mkSecretsArgs]
    &= program "silhouette"
    &= summary "Transpiles app specifications into Kubernetes configurations."

main = cmdArgs silhouetteModes >>= handleCmd
